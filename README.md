## nuxtjs-web-qiye

> 尝试Nuxtjs制作的企业站

### Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```


### 注意事项

> 配置全局公用less[需要less-loader不能超过7.0.0]  https://www.jianshu.com/p/522351e7a6a6
> 使用@nuxtjs/axios处理接口和代理：
    https://www.jianshu.com/p/36327c1f00f7
    https://blog.csdn.net/weixin_41852038/article/details/122115948
    https://blog.csdn.net/q95548854/article/details/107198570?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-1.pc_relevant_default&spm=1001.2101.3001.4242.2&utm_relevant_index=4
    https://www.cnblogs.com/ichenchao/articles/12191443.html

