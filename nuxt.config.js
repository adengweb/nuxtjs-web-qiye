export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Nuxtjs案例展示——adeng.y',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '阿灯前端开发博客网站、Nuxtjs案例展示、Vuejs，Reactjs，Uniapp，artTemplate、Element、AntD、Vant、rem适配、viewport适配及vh、vw适配、响应式布局和flex布局、jQuery、Zepto、swiper；掌握Javascript预加载模块框架seaJs、RequireJs、echarts图表，百度高德地图、localStorage与sessionStorage与cookie' },
      { name: 'Keywords', content: '阿灯前端博客网址、Vuejs、Reactjs、Uniapp、Element-ui、Vant-ui、ant-design、jquery、hbuildx、adeng.y、adeng.vip' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'referrer', content: 'no-referrer' },
      { name: 'author', content: 'adeng.y' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  // 配置全局公用less[需要less-loader不能超过7.0.0]  https://www.jianshu.com/p/522351e7a6a6
  css: [
    'element-ui/lib/theme-chalk/index.css',
    './static/css/common.less',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/element-ui'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios',],
  axios: {
    // 开启代理 (如果需要判断线上线下环境，可以通过 process.env.NODE_ENV !== 'production' 来判断)
    proxy: true,
    // 给请求 url 加个前缀 /api，如果这项不配置，则需要手动添加到请求链接前面
    // 如果是多个代理的时候，则不需要配置，走手动添加代理前缀
    prefix: '/api',
    // 跨域请求时是否需要使用凭证
    credentials: true
  },
  proxy: {
    '/api': {
      // 目标接口域
      target: 'http://www.adeng.vip/api',
      // 全局配置是否跨域
      changeOrigin: true,
      pathRewrite: {
        // 单个配置是否跨域
        // changeOrigin: true
        // 把 '/api' 替换成 '/'，具体需要替换为 '' 还是 '/' 看自己习惯
        '^/api': ''
      }
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build/
  // 打包命令 npm run generate
  build: {
    // 防止重复打包
    vendor: ['axios'],
    transpile: [/^element-ui/],
  },
}
